describe("GUI elements selection Demo", ()=>{
    it("Register page", () =>{
        cy.visit("https://demo.nopcommerce.com/")
        cy.get(".ico-register").click()
        cy.get("input[value='M']").should("be.visible").should("not.be.checked")
        cy.get("input[value='F']").should("be.visible").should("not.be.checked")
        cy.get("input[value='M']").click()

        cy.get("#Newsletter").check().should("be.checked").and("have.value","true")
        cy.get("#Newsletter").uncheck().should("not.be.checked")

        cy.get("[name='DateOfBirthDay']").select("8").should("have.value","8")
        cy.get("[name='DateOfBirthMonth']").select("June").should("have.value","6")

    })
})
