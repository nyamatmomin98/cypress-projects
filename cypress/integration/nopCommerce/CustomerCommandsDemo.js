//// <reference types="cypress" />

describe("Customer commands Demo", function () {
  it("Test login", function () {
    cy.login("admin@yourstore.com", "admin");
    cy.title().should("eq", "Dashboard / nopCommerce administration");
  });

  it("Test add Customer", function () {
    cy.login("admin@yourstore.com", "admin");
    cy.title().should("eq", "Dashboard / nopCommerce administration");
    cy.log("-------------Added a customer---------------------");
  });

  it("Test Edit Customer", function () {
    cy.login("admin@yourstore.com", "admin");
    cy.title().should("eq", "Dashboard / nopCommerce administration");
    cy.log("-------------Edited a customer---------------------");
  });
});
