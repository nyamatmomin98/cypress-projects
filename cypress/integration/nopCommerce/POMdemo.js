//// <reference types="cypress" />

import LoginPage from "./Project Object Model/LoginPage";

describe("Demo on POM", function () {
  it("Login validation", function () {
    const lp = new LoginPage();

    lp.visit();
    lp.fillEmail("admin@yourstore.com");
    lp.fillPass("admin");
    lp.submit();
  });
});
