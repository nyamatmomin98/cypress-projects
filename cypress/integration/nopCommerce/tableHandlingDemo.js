//  <reference types="cyrpess" />

describe("Table handling Demo", function(){
    
    it("Find values in table", function(){
        cy.visit("http://testautomationpractice.blogspot.com/")
        cy.get("table[name='BookTable']").contains("td", "Learn Selenium").should("be.visible")

        cy.get("table[name='BookTable'] > tbody > tr:nth-child(5) > td:nth-child(2)")
        .contains("Mukesh").should("be.visible")

        cy.get("table[name='BookTable'] > tbody > tr td:nth-child(2)").each(($e, index, $list) =>{

            const author = $e.text()
            if(author.includes("Amod"))
            {
                cy.get("table[name='BookTable'] > tbody > tr td:nth-child(1)").eq(index).then(function(bname){
                    const bookName = bname.text()
                    expect(bookName).to.equal("Master In Selenium")
                })
            }
        })
        
    })
})