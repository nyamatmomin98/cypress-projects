////<reference types="cypress" />

class LoginPage
{

    visit()
    {
        cy.visit("https://admin-demo.nopcommerce.com/login?ReturnUrl=%2Fadmin%2F")
    } 

    fillEmail(email)
    {
        const emailElement = cy.get("#Email")
        emailElement.clear()
        emailElement.type(email)
        return this
    }

    fillPass(pwd)
    {
        const passElement = cy.get("#Password")
        passElement.clear()
        passElement.type(pwd)
        return this
    }

    submit()
    {
        cy.get('button[type=submit]').click()
    } 
}

export default LoginPage