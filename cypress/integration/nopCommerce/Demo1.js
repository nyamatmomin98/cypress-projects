
describe('Demo on asserting title', () =>{

    it('should get and assert home page title', () =>{

        cy.visit("https://demo.nopcommerce.com/")
        cy.title().should('eq', 'nopCommerce demo store')
        cy.get('.ico-login').click() 
        cy.go('back')
    
    })
}
)