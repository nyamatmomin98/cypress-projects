//// <reference types="cypress" />

describe("Demo on fixtures", function(){

    before(function(){
        cy.visit("https://admin-demo.nopcommerce.com/login?ReturnUrl=%2Fadmin%2F")
        cy.fixture("loginCred.json").then(function(data){
            this.data = data
        })

    }) 

    it("login", function(){
        cy.get("#Email").clear().type(this.data.email)
        cy.get("#Password").clear().type(this.data.password)
        cy.get("button[type=submit]").click()
        cy.title().should('eq','Dashboard / nopCommerce administration')

    }) 
})